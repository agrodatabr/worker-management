<img src="serasa-logo.png"  alt=""/>
<h3>Serasa Reader</h3>
Pacote para realizar leitura de arquivos do Serasa via integração SFTP.


### Diagrama de classes/Fluxo
<img src="diagram.png"></img> <br>
O pacote possui 2 serviços que herdam a conexão SFTP: O gravador de arquivo (Writer) e o leitor de Arquivos (Reader).
Esses métodos são responsáveis pela consulta e escrita de arquivos no SFTP do Serasa de acordo com a [documentação](https://agrodata.atlassian.net/wiki/spaces/COF/pages/1217396737/Integra+o+Serasa#4.2-RECEBIMENTO-(leitura-arquivo-de-retorno))
recebida do Serasa.


### Instalação
#### Laravel
Instale o pacote com o seguinte comando:

```
composer require agrodata/serasa-reader
```

Registre o Provider no arquivo `config/app.php`
```php
<?php #config/app.php
    ...
    'providers' => [
        Agrodata\SerasaServiceProvider::class
    ],
    ...
```


#### Lumen

Registre o provider no arquivo ```bootstrap/app.php```

Adicione a seguinte linha na seção "Register Service Providers" no fim do arquivo.

```php
<?php #bootstrap/app.php
$app->register(\Agrodata\SerasaServiceProvider::class);
```
Descomente a linha ```$app->withFacades();``` no arquivo ```bootstrap/app.php```

### Configuration
Copie o arquivo no diretório `vendor/agrodata/serasa-reader/config/serasa-sftp.php` ou o código abaixo para a pasta de configuração local `config/serasa-sftp.php` para sobrescrever as configurações padrões da biblioteca.

```php
<?php #config/serasa-sftp.php

return [
    /**
     * homolog: 200.245.207.128
     * prod: 200.245.207.128
     */
    'host'             => env('SERASA_SFTP_HOST', '200.245.207.128'),
    'port'             => env('SERASA_SFTP_PORT', 8022),
    'username'         => env('SERASA_SFTP_USER'),
    'password'         => env('SERASA_SFTP_PASS'),
    'privateKey'       => env('SERASA_SFTP_PRIVATEKEY'),
    'my_cnpj'          => env("SERASA_SFTP_MY_CNPJ"),
    'my_social_reason' => env("SERASA_SFTP_MY_SOCIAL_REASON"),
    'logon'            => env("SERASA_SFTP_LOGON"),
    'gerencie-pf' => [
        'profile' => env('SERASA_SFTP_PF_PROFILE_NAME', ""),
        'remote-path' => env('SERASA_SFTP_PF_REMOTE_PATH', "797-CREDIT_BUREAU_TESTE"),
    ],
    'gerencie-pj' => [
        'profile' => env('SERASA_SFTP_PJ_PROFILE_NAME', ""),
        'remote-path' => env('SERASA_SFTP_PJ_REMOTE_PATH', "861-GERENCIE_TESTE"),
    ]
];

```

### Testes
#### Conexão
```php
<?php

(new \Agrodata\Serasa\Connection\TestSftpConnection)->test(); #retorna boolean
```

#### Inclusão/Exclusão de Arquivos
Para Incluir/Excuir Pesssoas/Empresas do Serasa utilize um dos métodos abaixo.
Os métodos aceitam tanto CPF ou CNPJ, com ou sem formatação/mascara..
após o envio o método irá criar o arquivo de inclusão/exclusão na pasta de integração
do Serasa com o formato especificado na [documentação](https://agrodata.atlassian.net/wiki/spaces/COF/pages/1217396737/Integra+o+Serasa#4.2-RECEBIMENTO-(leitura-arquivo-de-retorno))

```php
<?php

(new \Agrodata\Serasa\SerasaService())->include("09988521630")
#and
(new \Agrodata\Serasa\SerasaService)->exclude("09988521630");
```

### Testes Locais

Exemplo de leitura de arquivo de retorno para Pessoa Jurídica
```php
<?php

    (new Agrodata\Serasa\Read\GerenciePJ())->toArray(fopen('C:\Users\naira\Desktop\R.085.L5709.IPMONITO.RET.D230207.H194615.txt', 'r'))
```

Criar arquivo com resultado da leitura do arquivo do serasa
```php
<?php
    file_put_contents("file.json", json_encode((new Agrodata\Serasa\Read\GerenciePJ)->toArray(fopen('C:\Users\naira\Downloads\R.085.L5709.IPMONITO.RET.D230327.H204228.TXT', 'r'), true)))
```