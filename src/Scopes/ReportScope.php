<?php

namespace Agrodata\WorkerManagement\Scopes;

use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Builder;
use Agrodata\WorkerManagement\Models\Worker;

class ReportScope implements Scope
{
    public function apply(Builder $builder, $model)
    {
        $builder
            ->where('worker_type', '=', Worker::TYPE_REPORT)
            ->orderBy('created_at', 'DESC');
    }
}
