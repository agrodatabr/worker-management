<?php

namespace Agrodata\WorkerManagement\Scopes;

use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Builder;
use Agrodata\WorkerManagement\Models\Worker;

class ImportScope implements Scope
{
    public function apply(Builder $builder, $model)
    {
        $builder
            ->where('worker_type', '=', Worker::TYPE_IMPORT)
            ->orderBy('created_at', 'DESC');
    }
}
