<?php

namespace Agrodata\WorkerManagement\Console;

use Illuminate\Console\Command;

class InstallCommand extends Command
{
    protected $signature = 'worker-management:install';
    protected $description = 'Create migrations file of the Worker manager';

    private string $basePath;
    private string $workerRoutePath;

    public function __construct()
    {
        parent::__construct();
        $this->basePath = __DIR__. "/../..";
        $this->workerRoutePath = base_path("/routes");
    }

    public function handle()
    {
        $this->output->info('Copying migrations files');
        $this->registerMigrations();

        $this->output->info('Copying route files');
        $this->registerRoutes();

        $this->output->warning("Don't forget to register the routes in the api file [routes/api.php]");
        exit;
    }

    protected function registerMigrations()
    {
        $migrationDateReplacer = [
            '2023_01_00_000000',
            '2023_01_01_000000',
            '2023_01_02_000000',
            '2023_01_03_000000',
            '2023_01_04_000000',
            '2023_01_05_000000',
            '2023_01_06_000000',
            '2023_01_07_000000',
        ];
        $migrationVendorFiles = $this->migrationVendorFiles();
        $migrationProjectFiles = $this->migrationProjectFiles();

        foreach ($migrationVendorFiles as $counter => $file) {
            $replacedName = str_replace($migrationDateReplacer, "", $file);
            $fakeName = "[timestamp]".$replacedName;
            $exists = false;

             foreach($migrationProjectFiles as $projectFile) {
                if (str_contains($projectFile, $replacedName)) {
                    $this->warn(
                        "> [ALREADY EXISTS] /worker-manager/{$fakeName} -> /database/migrations/{$fakeName}"
                    );
                    $exists = true;
                    break;
                }
            }

            if (!$exists) {
                $newFileName = str_replace(
                    $migrationDateReplacer,
                    now()->addSeconds($counter)->format('Y_m_d_His'),
                    $file
                );

                file_put_contents(
                    database_path('migrations') . DIRECTORY_SEPARATOR . $newFileName,
                    file_get_contents("$this->basePath/database/migrations/$file")
                );

                $this->line("<fg=green>> [COPIED] /worker-manager/$fakeName -> /database/migrations/$fakeName");
            }
        }
    }

    protected function registerRoutes()
    {
        $routeVendorFiles = $this->routeVendorFiles();
        $routeProjectFiles = $this->routeProjectFiles();

        foreach ($routeVendorFiles as $file) {
            $exists = false;
            foreach ($routeProjectFiles as $projectFile) {
                if (str_contains($projectFile, $file)) {
                    $this->warn("> [ALREADY EXISTS] /worker-manager/routes/$file -> /routes/$file ");
                    $exists = true;
                    break;
                }
            }

            if (!$exists) {
                file_put_contents(
                    $this->workerRoutePath . DIRECTORY_SEPARATOR . $file,
                    file_get_contents("{$this->basePath}/routes/$file")
                );
                $this->line("<fg=green> > [COPIED] /worker-manager/routes/{$file} -> /routes/worker/{$file}", );
            }
        }
    }

    private function routeVendorFiles(): array
    {
        return $this->getFilesPath('/routes');
    }

    private function routeProjectFiles(): array
    {
        if (!is_dir($this->workerRoutePath)) {
            mkdir($this->workerRoutePath);
        }
        return array_filter(scandir($this->workerRoutePath), fn ($fileName) => !in_array($fileName, ['.', '..']));
    }

    private function migrationVendorFiles(): array
    {
        return $this->getFilesPath('/database/migrations');
    }

    private function migrationProjectFiles(): array
    {
        return array_filter(
            scandir(database_path('migrations')),
            fn ($fileName) => !in_array($fileName, ['.', '..'])
        );
    }

    private function getFilesPath(string $path): array
    {
        return array_filter(
            scandir($this->basePath.$path),
            fn ($fileName) => !in_array($fileName, ['.', '..'])
        );
    }
}
