<?php

namespace Agrodata\WorkerManagement;

use Illuminate\Support\ServiceProvider;
use Agrodata\WorkerManagement\Console\InstallCommand;

class WorkerManagementServiceProvider extends ServiceProvider
{
    public function boot()
    {
    }

    public function register()
    {
        $this->commands([
            InstallCommand::class
        ]);
    }
}
