<?php

namespace Agrodata\WorkerManagement\ModelFilters;

use EloquentFilter\ModelFilter;

class WorkerGroupFilter extends ModelFilter
{
    public function name($value)
    {
        return $this->whereRaw("LOWER(name) = '%".strtolower($value)."%'");
    }

    public function origin($value)
    {
        return $this->where('origin', strtolower($value));
    }

    public function destiny($value)
    {
        return $this->where('destiny', strtolower($value));
    }

    public function search($search)
    {
        $search = strtoupper($search);
        $this
            ->whereRaw("UPPER(name) like '%{$search}%'")
            ->orwhereRaw("UPPER(origin) like '%{$search}%'")
            ->orwhereRaw("UPPER(destiny) like '%{$search}%'");
    }
}
