<?php

namespace Agrodata\WorkerManagement\ModelFilters;

use EloquentFilter\ModelFilter;

class WorkerFilter extends ModelFilter
{
    public function workerType(string $value)
    {
        return $this->where("worker_type", strtolower($value));
    }

    public function groupName($value)
    {
        return $this->whereHas("group", fn ($group) =>
            $group->whereRaw("LOWER(name) ILIKE '%".strtolower($value)."%'"));
    }

    public function origin($value)
    {
        return $this->whereHas("group", fn ($group) =>
            $group->where('origin', strtolower($value)));
    }

    public function destiny($value)
    {
        return $this->whereHas("group", fn ($group) =>
            $group->where('destiny', strtolower($value)));
    }

    public function status($value)
    {
        return $this->where("status", strtolower($value));
    }

    public function group($value)
    {
        return $this->where("worker_group_id", $value);
    }
}
