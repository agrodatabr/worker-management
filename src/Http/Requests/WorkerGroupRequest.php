<?php

namespace Agrodata\WorkerManagement\Http\Requests;

use App\Util\Response;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

class WorkerGroupRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules()
    {
        $id = $this->route()->originalParameter('workerGroup') ?? 'NULL';
        return [
            'name' => "string|required|unique:worker_groups,name,$id,id",
            'origin' => "string|required",
            'destiny' => "string|required"
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(self::output('error', 'The information entered is invalid', 417, $errors));
    }

    private static function output(string $status = 'success', string $message = "", int $httpCode = 200, $data = [])
    {
        return response()->json(
            [
                'status'  => $status,
                'message' => $message,
                ($status === 'error'? 'errors': 'data')  => $data,
            ],
            $httpCode
        );
    }
}
