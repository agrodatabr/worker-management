<?php
namespace Agrodata\WorkerManagement\Http\Requests;

use App\Util\Response;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\ValidationException;

class WorkerRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules()
    {
        return [
            'status' => 'string',
            'worker_group_id' => 'uuid|exists:worker_groups,id|required',
            'worker_type' => 'string|required',
            'description' => 'string|nullable',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(response()->json($errors, 417));
    }
}
