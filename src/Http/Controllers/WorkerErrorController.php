<?php

namespace Agrodata\WorkerManagement\Http\Controllers;

use Agrodata\WorkerManagement\Models\WorkerError;
use Illuminate\Http\Response;

class WorkerErrorController extends BaseController
{
    public function index(string $importId): Response
    {
        return response(WorkerError::findByWorkerId($importId)->paginate($this->perPage));
    }
}
