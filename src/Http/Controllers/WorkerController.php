<?php

namespace Agrodata\WorkerManagement\Http\Controllers;

use Agrodata\WorkerManagement\Models\Worker;
use Agrodata\WorkerManagement\Http\Requests\WorkerRequest;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

class WorkerController extends BaseController
{
    public function index(Request $request): Response
    {
        return response((new Worker)->filter($request->all())
            ->orderByDesc('created_at')
            ->paginate($this->perPage));
    }

    public function show(Worker $worker): Response
    {
        return response($worker);
    }

    public function store(WorkerRequest $request, string $type): Response
    {
        return response(Worker::create([...$request->validated(), 'type_id' => $type]));
    }

    public function update(WorkerRequest $request, Worker $worker): Response
    {
        return response($worker->update($request->validated()));
    }

    public function destroy(Worker $worker): Response
    {
        return response($worker->delete());
    }
}
