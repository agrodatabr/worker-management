<?php

namespace Agrodata\WorkerManagement\Http\Controllers;

use Agrodata\WorkerManagement\Models\WorkerGroup;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Agrodata\WorkerManagement\Http\Requests\WorkerGroupRequest;

class WorkerGroupController extends BaseController
{
    public function index(Request $request): Response
    {
        return response(WorkerGroup::filter($request->all())->orderByDesc('created_at')->paginate($this->perPage));
    }

    public function store(WorkerGroupRequest $request): Response
    {
        return response(WorkerGroup::create($request->validated()), 201);
    }

    public function update(WorkerGroupRequest $request, WorkerGroup $workerGroup): Response
    {
        if ($workerGroup->update($request->validated())) {
            return response($workerGroup);
        }
        return response('', 500);
    }

    public function delete(string $workerGroup): Response
    {
        return response(WorkerGroup::find($workerGroup)->delete());
    }
}
