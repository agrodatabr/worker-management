<?php

namespace Agrodata\WorkerManagement\Http\Controllers;

use Agrodata\WorkerManagement\Models\Report;
use Agrodata\WorkerManagement\Http\Requests\WorkerRequest;
use Illuminate\Http\Response;

class ReportController extends BaseController
{
    public function index(WorkerRequest $request): Response
    {
        return response(Report::filter($request->all())->paginate($this->perPage));
    }

    public function show(Report $report): Response
    {
        return response($report);
    }
}
