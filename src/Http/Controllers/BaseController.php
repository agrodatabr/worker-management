<?php

namespace Agrodata\WorkerManagement\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

abstract class BaseController extends Controller
{
    protected int $perPage;

    public function __construct(Request $request)
    {
        $this->perPage = $request->per_page ?? config('pagination.default');
    }
}
