<?php

namespace Agrodata\WorkerManagement\Http\Controllers;

use Agrodata\WorkerManagement\Http\Requests\WorkerGroupUserRequest;
use Agrodata\WorkerManagement\Models\WorkerGroupUser;
use Agrodata\WorkerManagement\Models\WorkerGroup;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class WorkerGroupUserController extends BaseController
{
    public function index(Request $request, string $groupId): Response
    {
        $users = DB::table('users')->select(['id', 'name', 'email'])->orderBy('name');
        $users = ($request->search
            ? $users->whereRaw("UPPER(name) LIKE UPPER('%{$request->search}%')")
            : $users
        );
        return response(($request->users_not_in_group
            ? $users->whereNotIn('id', $this->getUsersByGroupId($groupId))
            : $users->whereIn('id', $this->getUsersByGroupId($groupId))
        )->paginate($this->perPage));
    }

    public function store(WorkerGroupUserRequest $request, WorkerGroup $workerGroup): Response
    {
        $workerGroup->users()->sync($request->users, false);
        return response('', 201);
    }

    public function delete(WorkerGroupUserRequest $request, WorkerGroup $workerGroup): Response
    {
        return response($workerGroup->users()->detach($request->users));
    }

    private function getUsersByGroupId(string $groupId)
    {
        return WorkerGroupUser::where('worker_group_id', $groupId)->pluck('user_id');
    }
}
