<?php

namespace Agrodata\WorkerManagement\Http\Controllers;

use Agrodata\WorkerManagement\Models\ImportExport;
use Agrodata\WorkerManagement\Http\Requests\WorkerRequest;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

class ImportExportController extends BaseController
{
    public function index(Request $request): Response
    {
        return response(ImportExport::filter($request->all())->paginate($this->perPage));
    }

    public function show(ImportExport $importExport): Response
    {
        return response($importExport);
    }

    public function store(WorkerRequest $request)
    {
        return response(ImportExport::create($request->validated()));
    }

    public function update(WorkerRequest $request, ImportExport $importExport)
    {
        return response($importExport->update($request->validated()));
    }
}
