<?php

namespace Agrodata\WorkerManagement\Enums;

enum WorkerTypeEnum: string
{
    const string REPORT = 'report';
    const string EXPORT = 'export';
    const string IMPORT = 'import';
}
