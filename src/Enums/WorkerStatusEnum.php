<?php

namespace Agrodata\WorkerManagement\Enums;

enum WorkerStatusEnum: string
{
    const string PROCESSED  = 'processed';
    const string PROCESSING = 'processing';
    const string FAILED     = 'failed';
}
