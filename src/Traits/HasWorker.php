<?php

namespace Agrodata\WorkerManagement\Traits;

use Agrodata\WorkerManagement\Models\ModelHasWorker;
use Agrodata\WorkerManagement\Models\WorkerError;
use App\Model\ImportError;
use Illuminate\Database\Eloquent\Relations\HasOneThrough;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Agrodata\WorkerManagement\Models\Import;

/**
 * Relacionamentos para usar nas models que possuem ligaçao com a tabela workers/imports
 */
trait HasWorker
{
    public function imports(): MorphToMany
    {
        return $this->morphToMany(
            Import::class,
            'model',
            'model_has_workers',
            'model_id',
            'worker_id'
        );
    }


    public function importErrors(): MorphToMany
    {
        return $this->morphToMany(
            WorkerError::class,
            'model',
            'model_has_workers',
            'model_id',
            'worker_id',
            'id',
            'worker_id'
        );
    }

    public function lastImport(): HasOneThrough
    {
        return $this->hasOneThrough(
            Import::class,
            ModelHasWorker::class,
            'model_id',
            'id',
            'id',
            'worker_id'
        );
    }

    public function lastImportError(): HasOneThrough
    {
        return $this->hasOneThrough(
            WorkerError::class,
            ModelHasWorker::class,
            'model_id',
            'worker_id',
            'id',
            'worker_id'
        );
    }

    public function lastImportByGroup(string $workerGroupId): HasOneThrough
    {
        return $this
            ->lastImport()
            ->where('workers.worker_group_id', $workerGroupId);
    }

    public function lastImportErrorByGroup(string $workerGroupId): HasOneThrough
    {
        return $this
            ->lastImportError()
            ->join('workers', 'workers.id', 'model_has_workers.worker_id')
            ->where('workers.worker_group_id', $workerGroupId);
    }
}
