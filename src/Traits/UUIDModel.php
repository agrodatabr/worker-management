<?php

namespace Agrodata\WorkerManagement\Traits;

use Illuminate\Support\Facades\Schema;
use Ramsey\Uuid\Uuid;

trait UUIDModel
{
    protected static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            $key = $model->getKeyName();

            if (empty($model->{$key})) {
                $model->{$key} = (string) Uuid::uuid4();
            }
            if (Schema::hasColumn($model->getTable(), 'user_id')) {
                $model->user_id = auth()->id();
            }
        });
    }
}
