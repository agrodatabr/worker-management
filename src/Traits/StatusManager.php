<?php

namespace Agrodata\WorkerManagement\Traits;

trait StatusManager
{
    protected string $statusProcessing = 'processing';
    protected string $statusProcessed = 'processed';
    protected string $statusFailed = 'failed';

    protected static function booted()
    {
        parent::booted();
        static::creating(function ($model) {
            if (empty($model->status)) {
                $model->status = 'processing';
            }
        });
    }

    protected function changeStatus(string $status): bool
    {
        return $this->update(['status' => $status]);
    }

    public function failed(): bool
    {
        return $this->changeStatus($this->statusFailed);
    }

    public function processing(): bool
    {
        return $this->changeStatus($this->statusProcessing);
    }

    public function processed(): bool
    {
        return $this->changeStatus($this->statusProcessed);
    }
}
