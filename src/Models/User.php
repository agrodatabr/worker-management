<?php

namespace Agrodata\WorkerManagement\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    public $incrementing = false;
    protected $keyType = 'string';
    protected $primaryKey = 'id';
    public $hidden = ['pivot'];
}
