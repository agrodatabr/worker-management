<?php

namespace Agrodata\WorkerManagement\Models;

use Agrodata\WorkerManagement\Scopes\ReportScope;

class Report extends Worker
{
    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new ReportScope);
        static::creating(fn () => $this->worker_type = self::TYPE_REPORT);
    }
}
