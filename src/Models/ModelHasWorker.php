<?php

namespace Agrodata\WorkerManagement\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class ModelHasWorker extends Pivot
{
    protected $table = 'model_has_workers';
}
