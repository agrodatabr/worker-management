<?php

namespace Agrodata\WorkerManagement\Models;

use Agrodata\WorkerManagement\Enums\WorkerTypeEnum;
use Agrodata\WorkerManagement\Traits\UUIDModel;
use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;
use Agrodata\WorkerManagement\Traits\StatusManager;
use Agrodata\WorkerManagement\ModelFilters\WorkerFilter;
use Agrodata\WorkerManagement\Models\WorkerError;
use Kyslik\ColumnSortable\Sortable;

class Worker extends Model
{
    use UUIDModel, StatusManager, Filterable, Sortable;

    public $incrementing = false;
    protected $primaryKey = 'id';

    protected $table = 'workers';
    protected $keyType = 'string';

    public const TYPE_REPORT = WorkerTypeEnum::REPORT;
    public const TYPE_EXPORT = WorkerTypeEnum::EXPORT;
    public const TYPE_IMPORT = WorkerTypeEnum::IMPORT;

    protected $fillable = [
        'user_id',
        'worker_group_id',
        'worker_type',
        'status',
	    'description'
    ];

    public function modelFilter()
    {
        return $this->provideFilter(WorkerFilter::class);
    }

    public function errors()
    {
        return $this->hasMany(WorkerError::class, 'worker_id', 'id');
    }

    public function group()
    {
        return $this->belongsTo(WorkerGroup::class, 'worker_group_id');
    }

    public function morph()
    {
        return $this->morphTo('model');
    }
}
