<?php

namespace Agrodata\WorkerManagement\Models;

use Agrodata\WorkerManagement\Scopes\ImportExportScope;
use MichaelAChrisco\ReadOnly\ReadOnlyTrait;

class ImportExport extends Worker
{
    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new ImportExportScope);
    }
}
