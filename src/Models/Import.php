<?php

namespace Agrodata\WorkerManagement\Models;

use Agrodata\WorkerManagement\Scopes\ImportScope;

class Import extends Worker
{
    public function __construct(array $attributes = [])
    {
        parent::__construct([
            ...$attributes,
            'worker_type' => self::TYPE_IMPORT
        ]);
    }

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new ImportScope);
    }
}
