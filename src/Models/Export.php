<?php

namespace Agrodata\WorkerManagement\Models;

use Agrodata\WorkerManagement\Scopes\ExportScope;

class Export extends Worker
{
    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new ExportScope);
        static::creating(fn () => $this->worker_type = self::TYPE_EXPORT);
    }
}
