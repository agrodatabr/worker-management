<?php

namespace Agrodata\WorkerManagement\Models;

use Agrodata\WorkerManagement\ModelFilters\WorkerGroupFilter;
use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;
use Agrodata\WorkerManagement\Traits\UUIDModel;
use \Agrodata\WorkerManagement\Models\User;

class WorkerGroup extends Model
{
    use UUIDModel, Filterable;

    public $incrementing = false;
    protected $keyType = 'string';
    protected $primaryKey = 'id';

    protected $fillable = [
        'name',
        'origin',
        'destiny',
        'route'
    ];

    public function users()
    {
        return $this->belongsToMany(User::class, WorkerGroupUser::class, 'worker_group_id', 'user_id');
    }

    public function modelFilter()
    {
        return $this->provideFilter(WorkerGroupFilter::class);
    }
}
