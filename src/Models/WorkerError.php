<?php

namespace Agrodata\WorkerManagement\Models;

use Agrodata\WorkerManagement\Traits\UUIDModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;

class WorkerError extends Model
{
    use UUIDModel;

    public $incrementing = false;
    protected $keyType = 'string';
    protected $primaryKey = 'id';

    protected $fillable = [
        'description'
    ];

    public static function findByWorkerId(string $id)
    {
        return self::where('worker_id', $id);
    }
}
