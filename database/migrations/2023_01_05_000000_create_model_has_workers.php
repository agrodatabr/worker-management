<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::create('model_has_workers', function (Blueprint $table) {
            $table->uuid('worker_id');
            $table->uuidMorphs('model');
        });
    }

    public function down()
    {
        Schema::dropIfExists('model_has_workers');
    }
};
