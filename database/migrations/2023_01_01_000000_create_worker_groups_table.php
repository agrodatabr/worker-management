<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::create('worker_groups', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('name')->unique();
            $table->string('origin');
            $table->string('destiny');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('worker_groups');
    }
};

