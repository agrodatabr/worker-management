<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::create('worker_errors', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('worker_id');
            $table->text('description');
            $table->timestamps();

            $table->foreign('worker_id')
                ->references('id')
                ->on('workers');
        });
    }

    public function down()
    {
        Schema::dropIfExists('worker_errors');
    }
};
