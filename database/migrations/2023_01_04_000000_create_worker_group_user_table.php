<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::create('worker_group_user', function (Blueprint $table) {
            $table->uuid('worker_group_id');
            $table->uuid('user_id');
            $table->timestamps();

            $table
                ->foreign('worker_group_id')
                ->references('id')
                ->on('worker_groups');

            $table
                ->foreign('user_id')
                ->references('id')
                ->on('users');
        });
    }

    public function down()
    {
        Schema::dropIfExists('worker_group_user');
    }
};
