<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::table('worker_groups', function (Blueprint $table) {
            $table->text('route')->nullable();
        });
    }

    public function down()
    {
        Schema::table('worker_groups', function (Blueprint $table) {
            if (\DB::getDefaultConnection() !== 'sqlite') {
                $table->dropColumn('route');
            }
        });
    }
};
