<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::create('workers', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('worker_group_id')->nullable();
            $table->string('worker_type');
            $table->string('user_id');
            $table->string('status');
            $table->timestamps();

            $table
                ->foreign('worker_group_id')
                ->references('id')
                ->on('worker_groups');
        });
    }

    public function down()
    {
        Schema::dropIfExists('workers');
    }
};
