<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::table('workers', function (Blueprint $table) {
            $table->text('description')->nullable();
        });
    }

    public function down()
    {
        Schema::table('workers', function (Blueprint $table) {
            if (\DB::getDefaultConnection() !== 'sqlite') {
                $table->dropColumn('description');
            }
        });
    }
};
