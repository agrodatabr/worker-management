<?php

use Agrodata\WorkerManagement\Http\Controllers\ImportExportController;
use Agrodata\WorkerManagement\Http\Controllers\ReportController;
use Agrodata\WorkerManagement\Http\Controllers\WorkerController;
use Agrodata\WorkerManagement\Http\Controllers\WorkerErrorController;
use Agrodata\WorkerManagement\Http\Controllers\WorkerGroupController;
use Agrodata\WorkerManagement\Http\Controllers\WorkerGroupUserController;
use Illuminate\Support\Facades\Route;

//group-users
Route::get('/worker/group/{workerGroup}/users', [WorkerGroupUserController::class, 'index']);
Route::post('/worker/group/{workerGroup}/users', [WorkerGroupUserController::class, 'store']);
Route::delete('/worker/group/{workerGroup}/users', [WorkerGroupUserController::class, 'delete']);

//groups
Route::get('/worker/group', [WorkerGroupController::class, 'index']);
Route::post('/worker/group', [WorkerGroupController::class, 'store']);
Route::put('/worker/group/{workerGroup}', [WorkerGroupController::class, 'update']);

//errors
Route::get('/worker/{worker}/errors', [WorkerErrorController::class, 'index']);

//imports
Route::get('/worker', [WorkerController::class, 'index']);
Route::get('/worker/{worker}', [WorkerController::class, 'show']);
Route::post('/worker/{type}', [WorkerController::class, 'store']);
Route::put('/worker/{worker}', [WorkerController::class, 'update']);
Route::delete('/worker/{worker}', [WorkerController::class, 'delete']);

//import-export filtered
Route::get('/import-export', [ImportExportController::class, 'index']);
Route::get('/import-export/{importExport}', [ImportExportController::class, 'show']);
Route::post('/import-export', [ImportExportController::class, 'store']);
Route::put('/import-export', [ImportExportController::class, 'update']);

//report filtered
Route::get('/report', [ReportController::class, 'index']);
Route::get('/report/{report}', [ReportController::class, 'show']);
